#!/bin/bash
#missing input parameters $FILENAME
#if title.txt exists
_update() {
    echo Updating title.txt file
    if [ -e title.txt ]
    then
        START=$( wc -l < title.txt)
        START=$(( START+1 ))
    #if not create
    else
        START=1
    fi

    END=$(ls -1 ./pdfs | tail -1)
    END=${END%.pdf}
    END=$(echo "$END" 0 -p | dc)
    END=$(( END+1))
    TOTAL=$(( (END-START)/100 ))

    if [ $TOTAL -eq 0 ]
    then
        TOTAL=$(( TOTAL+1 ))
    fi
    #if exist delete / if not put in error msg

    goBack="\r\r\r\r\r\r\r\r\r\r"
    printf "### Adding titles to title list in title.txt\n"

    COUNTER=$START
    while [ "$COUNTER" -lt "$END" ]; do
        printf "  %2.1f percent $goBack" $(( COUNTER / TOTAL ))
       TITLE=$(cat ./indexes/index$COUNTER.txt \
            | grep itemprop | head -1 \
            | sed -e 's/<h1 itemprop=\"name\">//' -e 's/<\/h1>//')
        printf "%4d %s\n" "$COUNTER" "$TITLE" >> title.txt
        let COUNTER=COUNTER+1
    done
    echo ""
    date +"%s" >> ./log/download.log
    dos2unix title.txt 1>> ./log/download.log
    echo DONE!
}
