#!/bin/bash

# check for dic struct and creates if missing
check_dir() {
    [ -d $1 ] && printf "DIR found: $1\n"
    [ ! -d $1 ] && printf "DIR not Found: $1\n" && printf "Creating DIR: $1\n" && mkdir $1
}

check_dir pdfs
check_dir images
check_dir cookies
check_dir indexes
check_dir log
echo "--- CHECKED ALL DIR ---"

# find last ebbok avaiable
last_available=$(python ./findLast.py)
last_available=${last_available##/book/}
last_available=${last_available%?}
printf "LAST FILE ON WEB: $last_available\n"

## find last downloaded ebook
last_downloaded=$(ls -1 ./pdfs | tail -1)
last_downloaded=${last_downloaded%.pdf}
last_downloaded=$(( 10#$last_downloaded ))
#last_downloaded=$(echo "$last_downloaded 0 -p" | dc)
if [ -z "$last_downloaded" ] ; then
    last_downloaded=0
fi
printf "LAST FILE ON DRIVE: $last_downloaded\n"

## get ebooks between last downloaded and last on site
#echo get-range.sh $last_downloaded $last_available
echo getting $((last_downloaded+1)) $((last_available+1))
#./get-range.sh $((last_downloaded+1)) $((last_available+1))

