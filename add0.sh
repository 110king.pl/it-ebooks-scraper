#!/bin/bash

path="./pdfs"

for filepath in ${path}/*.pdf
do
    filename=${filepath%.pdf}
    filename=${filename##*/}
    mv -u $filepath ${path}/$(printf "%05d.pdf\n" $((10#$filename)) )
done
