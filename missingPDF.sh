#!/bin/bash
echo $1
COUNTER=0
MISSING=0
while [ $COUNTER -lt $1 ]
do
    COUNTER=$(( COUNTER+1 ))
    FILENAME="./pdfs/"$COUNTER".pdf"
    if [ ! -f $FILENAME ]
    then
        printf "%s missing.\n" "$COUNTER"
        MISSING=$(( MISSING + 1))
    fi
done
echo $MISSING" Files missing ..."

