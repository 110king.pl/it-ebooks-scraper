#!/bin/bash

_download() {

    sec=$( date +"%s" )
    ERROR_LOG="./log/e-$sec.log"
    if [ $# -eq 2 ] ;then
        START=$1
        END=$2
    elif [ $# -eq 1 ] ; then
        START=$(ls ./indexes | wc -l)
        END=$1
    else
        printf "BAD INPUT ARG?"
        exit 3
    fi
    echo Downloading from "$1" to "$2"
    COUNTER=$START
    while [ $COUNTER -lt $END ]; do
       echo "### Getting Index $COUNTER"
       URL_STATUS=$(curl -I  --stderr /dev/null "http://it-ebooks.info/book/$COUNTER/" | head -1 | cut -d' ' -f2)
       wget --cookies=on --keep-session-cookies --save-cookies=cookie.txt http://it-ebooks.info/book/$COUNTER/ &> $ERROR_LOG
       mv cookie.txt ./cookies/$COUNTER.txt
       mv index.html ./indexes/index$COUNTER.txt
       if [ $URL_STATUS != "200" ]; then
            printf "%d does not exists\n" "$COUNTER"
            printf "%s URL response \n" "$URL_STATUS"
            COUNTER=$(( COUNTER + 1 ))
            continue
       fi
       # DOWNLOADING PDF
       DLINK=`cat ./indexes/index$COUNTER.txt | grep filepi | grep -o '<a href=['"'"'"][^"'"'"']*['"'"'"]' |sed -e 's/^<a href=["'"'"']//' -e 's/["'"'"']$//'`
       echo "### Downloading PDF $COUNTER"
       echo $DLINK
       wget -t 1 -T 1 -O ./pdfs/$COUNTER.pdf --referer=http://it-ebooks.info/book/$CPUNTER/ --cookies=on --load-cookies=./cookies/$COUNTER.txt --keep-session-cookies --save-cookies=./cookies/pi.$COUNTER.txt $DLINK
       # GETIING IMAGE
       ILINK=`cat ./indexes/index$COUNTER.txt | grep img | head -1 | sed -n 's/.*<img src="\([^"]*\)".*/\1/p'`
       echo "### Downloading IMAGE"
       echo $ILINK
       wget -T 10 -O ./images/$COUNTER.jpg --referer=http://it-ebooks.info/book/$CPUNTER/ http://www.it-ebooks.info$ILINK &> $ERROR_LOG
       # ADDING TITLE
       echo "### Adding Title to list"
       TITLE=`cat ./indexes/index$COUNTER.txt \
            | grep itemprop | head -1 \
            | sed -e 's/<h1 itemprop=\"name\">//' -e 's/<\/h1>//'`
        echo "$COUNTER $TITLE" >> ./log/lastTitle.txt
        let COUNTER=COUNTER+1
    done
}
