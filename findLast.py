from bs4 import BeautifulSoup
import urllib2

url = 'http://it-ebooks.info'
page = urllib2.urlopen(url)
soup = BeautifulSoup(page)
lastUpdate = soup.find_all('table')[0].find_all('tr')[4].tr.td.a['href']

print(lastUpdate)
